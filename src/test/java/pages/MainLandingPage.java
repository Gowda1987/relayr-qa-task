package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Main landing page of check24 web page.
 *
 */
public class MainLandingPage {

	private WebDriver driver;

	public MainLandingPage(WebDriver driver) {
		this.driver = driver;
	}

	By loginLink = By.xpath(".//*[@id='c24-customer-salutation']/a");

	/* Selects login link from Main page and returns login page */
	public LoginPage selectLoginPage() throws InterruptedException {
		driver.findElement(loginLink).click();
		LoginPage loginPage = new LoginPage(driver);
		return loginPage;

	}

}
