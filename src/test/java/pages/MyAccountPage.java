package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

/**
 * Verifies MyAccountPage URL.
 *
 */
public class MyAccountPage {

	private WebDriver driver;

	public MyAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	By logout = By.xpath(".//*[@id='c24-meinkonto']/div/div[2]/a");
	By myAccountHovor = By.xpath(".//*[@id='c24-meinkonto']/span/span[2]");
	By closeIcon = By.xpath(".//*[@id='c24-dialog-points-modal']/div/header/button");
	By loginPage = By.xpath(".//*[@id='c24-kb-container']/div/h2");

	/* Verifies registered user and logged in user Account page */
	public void assertAccountUrl() {
		String URL = driver.getCurrentUrl();
		System.out.println(URL);
		Assert.assertEquals(URL, "https://kundenbereich.check24.de/user/account.html");
	}

	/* Logs out from valid login and returns main login page */
	public LoginPage logOut() {
		driver.findElement(closeIcon).click();
		Actions action = new Actions(driver);
		WebElement myAccount = driver.findElement(myAccountHovor);
		action.moveToElement(myAccount).build().perform();
		driver.findElement(logout).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage));
		LoginPage loginPage = new LoginPage(driver);
		return loginPage;

	}

}
