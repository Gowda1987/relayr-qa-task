package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import junit.framework.Assert;

/**
 * Login page methods to perform Login flow.
 *
 */
public class LoginPage {

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	By registerCustomerLink = By.id("new_customer");
	By email = By.id("email");
	By password = By.id("password");
	By loginButton = By.id("c24-kb-register-btn");
	By errorMessage = By.xpath(".//*[@id='login']/div[2]/div[3]/div[1]");
	By passwordResetLink = By.id("pw_reset_btn");

	/* Selects registration link and returns registration page */
	public RegistrationPage registerNewCustomer() {
		driver.findElement(registerCustomerLink).click();
		RegistrationPage registrationPage = new RegistrationPage(driver);
		return registrationPage;
	}

	public void fillLoginForm(String emailId, String password) {
		enterEmail().sendKeys(emailId);
		enterPassword().sendKeys(password);
	}

	/* Logs in with valid credentials and returns MyAccount page */
	public MyAccountPage validLogin() {
		tapLoginButton();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(".//*[@id='c24-dialog-points-modal']/div/header/h1/span")));
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		return myAccountPage;

	}

	/* Logs in with invalid credentials */
	public void inValidlogin() {
		tapLoginButton();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessage));
	}

	/* Verifies error message from invalid credentials login */
	public void asserErrorPasswordMessage() {
		WebElement erroMessage = driver.findElement(errorMessage);
		String text = erroMessage.getText();
		String expectedText = "Bei der Eingabe Ihrer E-Mail-Adresse / Ihres Passworts ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.";
		Assert.assertEquals(text, expectedText);

	}

	/*
	 * Selects forgot password link from login page and returns forgot password
	 * page
	 */
	public ForgotPasswordPage forgetLoginLink() {
		driver.findElement(passwordResetLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='c24-kb-form-container']/div/h2")));
		ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage(driver);
		return forgotPasswordPage;
	}

	/* Enters EmailId */
	public WebElement enterEmail() {
		WebElement emailId = driver.findElement(email);
		return emailId;

	}

	/* Enters Password */
	public WebElement enterPassword() {
		WebElement passWord = driver.findElement(password);
		return passWord;
	}

	/* Click on Login button */
	public void tapLoginButton() {
		WebElement login = driver.findElement(loginButton);
		login.click();

	}

}
