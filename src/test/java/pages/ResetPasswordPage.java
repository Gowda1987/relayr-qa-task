package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;

/**
 * Verifies password reset flow.
 *
 */
public class ResetPasswordPage {
	private WebDriver driver;

	public ResetPasswordPage(WebDriver driver) {
		this.driver = driver;
	}

	By confirmmessage = By.xpath(".//*[@id='c24-content']/div/div[2]/div/h2");

	/* Verifies password reset page URL */
	public void assertResetPasswordConfirmMessage() {
		WebElement confirmMessage = driver.findElement(confirmmessage);
		String text = confirmMessage.getText();
		System.out.println(text);
		String expectedText = "E-Mail wurde versendet";
		Assert.assertEquals(text, expectedText);

	}

}
