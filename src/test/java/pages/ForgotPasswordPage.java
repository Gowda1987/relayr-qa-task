package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Forgot password page to verify forgot password link.
 *
 */
public class ForgotPasswordPage {

	private WebDriver driver;

	public ForgotPasswordPage(WebDriver driver) {
		this.driver = driver;
	}

	By email = By.id("email");
	By forgetPasswordButton = By.id("c24-kb-register-btn");

	/* Clicks on forgot password link and returns password reset page */
	public ResetPasswordPage resetPassword(String forgotEmail) {
		enterEmail().clear();
		enterEmail().sendKeys(forgotEmail);
		forgetPasswordButton().click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(".//*[@id='c24-content']/div/div[2]/div/div/p[2]/a")));
		ResetPasswordPage resetPasswordPage = new ResetPasswordPage(driver);
		return resetPasswordPage;

	}

	/* Enters EmailId */
	public WebElement enterEmail() {
		WebElement emailId = driver.findElement(email);
		return emailId;

	}

	/* Gets forgot password link */
	public WebElement forgetPasswordButton() {
		WebElement passwordButton = driver.findElement(forgetPasswordButton);
		return passwordButton;

	}

}