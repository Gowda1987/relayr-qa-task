package pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Registration page methods to perform registration flow.
 *
 */
public class RegistrationPage {

	private WebDriver driver;

	public RegistrationPage(WebDriver driver) {
		this.driver = driver;
	}

	By email = By.id("email");
	By password = By.id("password");
	By repeatPassword = By.id("passwordrepetition");
	By registerButton = By.id("c24-kb-btn");

	/* Fills Registration Page and returns MyAccount page */
	public MyAccountPage fillFormRegistrationPage() {
		enterEmail(generateEmail());
		enterPassword().sendKeys("Aa123456!");
		enterRepeatPassword().sendKeys("Aa123456!");
		tapRegisterButton();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(".//*[@id='c24-dialog-points-modal']/div/header/h1/span")));
		MyAccountPage myAccountpage = new MyAccountPage(driver);
		return myAccountpage;
	}

	/* Enters EmailId */
	public void enterEmail(String emailId) {
		WebElement emailID = driver.findElement(email);
		emailID.sendKeys(emailId);

	}

	/* Enters Password */
	public WebElement enterPassword() {
		return driver.findElement(password);
	}

	/* Enters Repeat Password */
	public WebElement enterRepeatPassword() {
		return driver.findElement(repeatPassword);
	}

	/* Click on Register button */
	public void tapRegisterButton() {
		WebElement registerBtn = driver.findElement(registerButton);
		registerBtn.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(".//*[@id='c24-dialog-points-modal']/div/header/h1/span")));
	}

	/* Generates random email */
	public String generateEmail() {
		Random rand = new Random();
		int randomNum = rand.nextInt((Integer.MAX_VALUE - 10000) + 1) + 10000;
		return "emailtest" + randomNum + "@trashmail.com";
	}

}
