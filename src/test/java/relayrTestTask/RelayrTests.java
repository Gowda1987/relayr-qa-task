package relayrTestTask;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.ForgotPasswordPage;
import pages.LoginPage;
import pages.MainLandingPage;
import pages.MyAccountPage;
import pages.RegistrationPage;
import pages.ResetPasswordPage;

/**
 * @author Asha Rani
 * 
 *         Tests to verify Login flow and registration flow in check24 web page.
 **/

public class RelayrTests {

	private WebDriver driver;
	
	private String validEmail = "test123@trashmail.com";
	private String validPassword = "Tt123456";
	private String invalidEmail = "test456@trashmail.com";
	private String invalidPassword = "Tt123456";

	/*
	 * Invokes FireFox Browser with Check24 default Landing Page, Launches
	 * browser and Enters Check24 Page after every test.
	 */
	@Before
	public void setup() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.check24.de/");
	}

	/* Test 1 : Registration flow, Registers New User. */
	@Test
	public void RegisterNewUser() throws InterruptedException {
		MainLandingPage mainLandingPage = new MainLandingPage(driver);
		mainLandingPage.selectLoginPage();
		LoginPage loginPage = new LoginPage(driver);
		loginPage.registerNewCustomer();
		RegistrationPage register = new RegistrationPage(driver);
		register.fillFormRegistrationPage();
		MyAccountPage myAccountpage = new MyAccountPage(driver);
		myAccountpage.assertAccountUrl();

	}

	/* Test 2 : Login flow, Log in with valid login credentials. */
	@Test
	public void LoginWithValidLoginCrdentials() throws InterruptedException {
		MainLandingPage mainLandingPage = new MainLandingPage(driver);
		mainLandingPage.selectLoginPage();
		LoginPage loginPage = new LoginPage(driver);
		loginPage.fillLoginForm(validEmail, validPassword);
		loginPage.validLogin();
		MyAccountPage myAccountpage = new MyAccountPage(driver);
		myAccountpage.assertAccountUrl();

	}

	/* Test 3 : Login flow, Log in with invalid login credentials. */
	@Test
	public void LoginWithInvalidLoginCrdentials() throws InterruptedException {
		MainLandingPage mainLandingPage = new MainLandingPage(driver);
		mainLandingPage.selectLoginPage();
		LoginPage loginPage = new LoginPage(driver);
		loginPage.fillLoginForm(invalidEmail, invalidPassword);
		loginPage.inValidlogin();
		loginPage.asserErrorPasswordMessage();

	}

	/*
	 * Test 4 : Verifies forgot password link with valid emailId in login page
	 * Enters the same Registered user for Forgot Password email.
	 */
	@Test
	public void ForgotPasswordLink() throws InterruptedException {
		MainLandingPage mainLandingPage = new MainLandingPage(driver);
		mainLandingPage.selectLoginPage();
		LoginPage loginpage = new LoginPage(driver);
		loginpage.registerNewCustomer();
		RegistrationPage register = new RegistrationPage(driver);
		String email = register.generateEmail();
		register.enterEmail(email);
		register.enterPassword().sendKeys(validPassword);
		register.enterRepeatPassword().sendKeys(validPassword);
		register.tapRegisterButton();
		MyAccountPage myAccountpage = new MyAccountPage(driver);
		LoginPage loginPage = myAccountpage.logOut();
		ForgotPasswordPage forgotPasswordPage = loginPage.forgetLoginLink();
		ResetPasswordPage resetPasswordPage = forgotPasswordPage.resetPassword(email);
		resetPasswordPage.assertResetPasswordConfirmMessage();

	}

	@After
	public void close() {
		driver.quit();
	}

}